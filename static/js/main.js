// JavaScript Document

(function () {
    "use strict";
    setTimeout(function(){
    $('#c-name').keyup(function(){
        var str = $(this).val();
        var ret = str.split(" ");
        var str1 = ret[0];
        var str2 = ret[1];
        $('#z-f-name').val(str1)
        $('#z-l-name').val(str2)
    })
    },500)
    
    var url = window.location.pathname;
    function gtag_report_conversion(url) {
        console.log(url);
        var callback = function () {
            if (typeof (url) != 'undefined') {
                window.location = url;
            }
        };
        gtag('event', 'conversion', {
            'send_to': 'AW-757527664/JGAZCLygqpkBEPDom-kC',
            'event_callback': callback
        });
        return false;
    }
    $('#rightnow').click(function () {
        $(this).parents('.callback-step1').addClass('d-none')
        $('.callback-step2').removeClass('d-none')
        $('#when-field').val('Right now')
    })
    $('#tomorrow').click(function () {
        $(this).parents('.callback-step1').addClass('d-none')
        $('.callback-step3').removeClass('d-none')
        $('#when-field').val('Tomorrow')
    })
    $('#continue').click(function () {
        $(this).parents('.callback-step3').addClass('d-none')
        $('.callback-step2').removeClass('d-none')
        $('#time-field').val($('#select-time').val())
    })


    if ($('.js-range-slider').length > 0) {
        var jsrangeslider = $(".js-range-slider")
        jsrangeslider.ionRangeSlider({
            skin: "round",
            min: 0,
            max: 20000,
            from: 1000,
            step: 1000,
            max_postfix: "+",
            hide_min_max: true,
            prefix: "£",
            onChange: function (data) {
                $('#debt-write').text('£' + data.from * 90 / 100)
                $('#owe-field').val(data.from)
            }
        });

        var debtfield = (1000 * 90 / 100)
        $('#debt-write').text('£' + debtfield)
        $('#owe-field').val(1000)

        $('#owe-field').keyup(function () {
            var jsrangeslider_instance = jsrangeslider.data("ionRangeSlider");
            var fromValue = $(this).val();
            $('#debt-write').text('£' + fromValue * 90 / 100)
            jsrangeslider_instance.update({
                from: fromValue,
            });
        })

    }

    $(window).mousedown(function () {
        $('input[name="url"]').val(window.location.pathname);
    });
    


})();
